#ifndef MATRIX_MATH_H_
#define MATRIX_MATH_H_

#include "FreeRTOS.h"
#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#define GENERATE_MATRIX_MATH(struct_name, type)									\
typedef struct																	\
{																				\
	uint32_t size_x;															\
	uint32_t size_y;															\
	type** data;																\
																				\
	void (*free)(void*);														\
}struct_name;																	\
																				\
static void struct_name##_free(void* matrix)									\
{																				\
	uint32_t i;																	\
	for( i = 0; i < ((struct_name*)matrix)->size_y; ++i)						\
		vPortFree(((struct_name*)matrix)->data[i]);								\
	vPortFree(((struct_name*)matrix)->data);									\
	vPortFree(matrix);															\
	matrix = NULL;																\
}																				\
																				\
static struct_name* struct_name##_construct(uint32_t x, uint32_t y)				\
{																				\
	uint8_t j = 0;																\
	struct_name* _newMatrix = (struct_name*)pvPortMalloc( sizeof(struct_name) );\
	if (_newMatrix == NULL)														\
		return NULL;															\
																				\
	_newMatrix->free = struct_name##_free;										\
	_newMatrix->size_x = x;														\
	_newMatrix->size_y = y;														\
	if ( ( _newMatrix->data = pvPortMalloc( y*sizeof(type*)) ) == NULL )		\
	{																			\
		vPortFree(_newMatrix);													\
		return NULL;															\
	}																			\
	memset(_newMatrix, 0x00, y*sizeof(type*));									\
																				\
	for (uint32_t i = 0; i < y; ++i)											\
	{																			\
		if (( _newMatrix->data[i] = pvPortMalloc( x * sizeof(type) )) == NULL )	\
		{																		\
			memset(_newMatrix->data[i], 0x00, x * sizeof(type));				\
			for(j = 0; j < i; ++j)												\
				vPortFree(_newMatrix->data[j]);									\
			vPortFree(_newMatrix);												\
			return NULL;														\
		}																		\
	}																			\
																				\
	return _newMatrix;															\
}																				\
																				\
static void struct_name##_mult_s(struct_name* m1, struct_name* m2, struct_name* result)	\
{																				\
	type sum = 0;																\
	/*by rows*/																	\
	for(uint32_t i = 0; i < m1->size_y; ++i)									\
	{	/*by columns*/															\
		for(uint32_t j = 0; j < m2->size_x; ++j)								\
		{																		\
			sum = 0;															\
			/*sum of multiplication of row*col*/								\
			for(uint32_t k = 0; k< m1->size_x; ++k)								\
				sum += m1->data[i][k] * m2->data[k][j];							\
																				\
			result->data[i][j] = sum;											\
		}																		\
	}																			\
}																				\
																				\
static struct_name* struct_name##_mult(struct_name* m1, struct_name* m2)		\
{																				\
	struct_name* result = NULL;													\
	type sum=0;																	\
	result = struct_name##_construct(m2->size_x, m1->size_y);					\
	for(uint32_t i = 0; i < m1->size_y;++i)										\
	{																			\
		for(uint32_t j = 0; j < m2->size_x; ++j)								\
		{																		\
			sum = 0;															\
																				\
			for(uint32_t k = 0; k < m1->size_x;++k)								\
				sum += m1->data[i][k] * m2->data[k][j];							\
																				\
		result->data[i][j] = sum;												\
		}																		\
	}																			\
	return result;																\
}																				\
																				\
static struct_name* struct_name##_mult_m(uint32_t n,...)						\
{																				\
	va_list arg;																\
	struct_name* arg1, *mult, *itr;												\
																				\
	if(n < 1)																	\
		return NULL;															\
																				\
	va_start(arg,n);															\
	arg1 = va_arg(arg, struct_name*);											\
	if(n == 1)																	\
	{																			\
		va_end(arg);															\
		return arg1;															\
	}																			\
	itr = va_arg(arg, struct_name*);											\
	mult = struct_name##_mult(arg1,itr);										\
	--n;																		\
	while(n--)																	\
	{																			\
		if(n == 0)																\
			break;																\
		itr = va_arg(arg, struct_name*);										\
		struct_name* prev = mult;												\
		mult = struct_name##_mult(mult,itr);									\
		struct_name##_free(prev);												\
	};																			\
	va_end(arg);																\
	return mult;																\
}																				\
																				\
static struct_name*  struct_name##_scalar(struct_name* m, type skalar)			\
{																				\
	if(m == NULL)																\
		return NULL;															\
																				\
	struct_name* result = struct_name##_construct(m->size_x, m->size_y);		\
	for(uint32_t i = 0; i < m->size_x; ++i)										\
		for(uint32_t j = 0; j < m->size_y; ++j)									\
			result->data[i][j] = m->data[i][j]*skalar;							\
	return result;																\
}																				\
																				\
static void struct_name##_scalar_s(struct_name* m, type skalar,					\
		struct_name* result)													\
{																				\
	if(m == NULL)																\
		return;																	\
																				\
	for(uint32_t i = 0; i < m->size_x; ++i)										\
		for(uint32_t j = 0; j < m->size_y; ++j)									\
			result->data[i][j] = m->data[i][j]*skalar;							\
}																				\
																				\
static struct_name* struct_name##_copy(struct_name* m)							\
{																				\
	if(m == NULL)																\
		return NULL;															\
	struct_name* result = struct_name##_construct(m->size_x,m->size_y);			\
	for(uint32_t i = 0; i < m->size_x; ++i)										\
		for(uint32_t j = 0; j < m->size_y; ++j)									\
			result->data[i][j] = m->data[i][j];									\
	return result;																\
}																				\
																				\
static struct_name* struct_name##_add(struct_name* m1, struct_name* m2)			\
{																				\
	struct_name* result = NULL;													\
	if((m1->size_x == m2->size_x) && (m1->size_y == m2->size_y))				\
	{																			\
		result = struct_name##_construct(m1->size_x, m1->size_y);				\
		/*by rows*/																\
		for(uint32_t i = 0; i < m1->size_y;++i)									\
			/*by columns*/														\
			for(uint32_t j = 0; j < m1->size_x; ++j)							\
				result->data[i][j] = m1->data[i][j] + m2->data[i][j];			\
	}																			\
	return result;																\
}																				\
																				\
static void struct_name##_add_s(struct_name* m1, struct_name* m2,				\
		struct_name* result)													\
{																				\
	if((m1->size_x == m2->size_x) && (m1->size_y == m2->size_y))				\
	{																			\
		/*by rows*/																\
		for(uint32_t i = 0; i < m1->size_y;++i)									\
			/*by columns*/														\
			for(uint32_t j = 0; j < m1->size_x; ++j)							\
				result->data[i][j] = m1->data[i][j] + m2->data[i][j];			\
	}																			\
}																				\
																				\
static struct_name* struct_name##_transpose(struct_name* m)						\
{																				\
	struct_name* result = NULL;													\
	result = struct_name##_construct(m->size_y, m->size_x);						\
	/*by rows*/																	\
	for(uint32_t i = 0; i < m->size_y;++i)										\
		/*by columns*/															\
		for(uint32_t j = 0; j < m->size_x; ++j)									\
			result->data[j][i] = m->data[i][j];									\
	return result;																\
}																				\
																				\
static void struct_name##_transpose_s(struct_name* m, struct_name* result)		\
{																				\
	/*by rows*/																	\
	for(uint32_t i = 0; i < m->size_y;++i)										\
		/*by columns*/															\
		for(uint32_t j = 0; j < m->size_x; ++j)									\
			result->data[j][i] = m->data[i][j];									\
}																				\
																				\
static struct_name* struct_name##_substract(struct_name* m1, struct_name* m2)	\
{																				\
	struct_name* result = NULL;													\
	if((m1->size_x == m2->size_x) && (m1->size_y == m2->size_y))				\
	{																			\
		result = struct_name##_construct(m1->size_x,m1->size_y);				\
		/*by rows*/																\
		for(uint32_t i = 0; i < m1->size_y;++i)									\
			/*by columns*/														\
			for(uint32_t j = 0; j < m1->size_x; ++j)							\
				result->data[i][j] = m1->data[i][j] - m2->data[i][j];			\
	}																			\
	return result;																\
}																				\
																				\
static void struct_name##_substract_s(struct_name* m1, struct_name* m2,			\
		struct_name* result)													\
{																				\
	if((m1->size_x == m2->size_x) && (m1->size_y == m2->size_y))				\
	{																			\
		/*by rows*/																\
		for(uint32_t i = 0; i < m1->size_y;++i)									\
			/*by columns*/														\
			for(uint32_t j = 0; j < m1->size_x; ++j)							\
				result->data[i][j] = m1->data[i][j] - m2->data[i][j];			\
	}																			\
}																				\
																				\
static struct_name* struct_name##_I(uint32_t size)								\
{																				\
	if(size == 0) 																\
		return NULL;															\
																				\
	struct_name* result = struct_name##_construct(size, size);					\
																				\
	for(uint32_t i = 0; i < result->size_y; ++i)								\
		result->data[i][i] = 1;													\
	return result;																\
}																				\
																				\
static void struct_name##_swap_biggest_down(struct_name* m, uint32_t i)			\
{																				\
	type biggest = 0;															\
	uint32_t biggest_num = 0;													\
	for(uint32_t k = i; k < m->size_y; ++k)										\
	{																			\
		type temp = m->data[k][i];												\
																				\
		if(temp < 0) 															\
			temp *= (-1);														\
																				\
		if(temp > biggest)														\
		{																		\
			biggest = temp;														\
			biggest_num = k;													\
		}																		\
	}																			\
																				\
	type old_value = 0;															\
																				\
	/*swap rows i , biggest_num	*/												\
	for(uint32_t k = 0; k < m->size_x; ++k)										\
	{																			\
		old_value = m->data[i][k];												\
		m->data[i][k] = m->data[biggest_num][k];								\
		m->data[biggest_num][k] = old_value;									\
	}																			\
}																				\
																				\
static struct_name* struct_name##_inv(struct_name* m)							\
{																				\
	if(m->size_x != m->size_y) 													\
		return NULL;															\
																				\
	struct_name* calc = struct_name##_construct(m->size_x*2,m->size_y);			\
	if(m->size_x == 1)															\
	{																			\
		calc->data[0][0] = 1 / m->data[0][0];									\
		return calc;															\
	}																			\
	/*copy m and apply I matrix*/												\
	for(uint32_t i = 0; i < m->size_x; ++i)										\
	{																			\
		calc->data[i][i+m->size_y] = 1;											\
		for(uint32_t j = 0; j < m ->size_y; ++j)								\
			calc->data[i][j] = m->data[i][j];									\
	}																			\
	for(uint32_t i = 0; i < calc->size_y; ++i)									\
	{																			\
		struct_name##_swap_biggest_down(calc,i);								\
		/*peculiar matrix*/														\
		if(calc->data[i][i] == 0)												\
		{																		\
			struct_name##_free(calc);											\
			return NULL;														\
		}																		\
		for(uint32_t j = 1+i; j < calc->size_y; ++j)							\
		{																		\
			type factor = calc->data[j][i]/calc->data[i][i];					\
																				\
			/*substract rows*/													\
			for(uint32_t k = 0; k < calc->size_x; ++k)							\
				calc->data[j][k] = calc->data[j][k] - (calc->data[i][k]*factor);\
		}																		\
	}																			\
																				\
	for(uint32_t i = calc->size_y; i-1 != 0; --i)								\
	{																			\
		/*peciliar matrix*/														\
		if(calc->data[i-1][i-1] == 0)											\
		{																		\
			struct_name##_free(calc);											\
			return NULL;														\
		}																		\
		for(uint32_t j = i - 1; j !=0; --j)										\
		{																		\
			type factor = calc->data[j-1][i-1]/calc->data[i-1][i-1];			\
																				\
			/*substract rows*/													\
			for(uint32_t k = 0; k < calc->size_x; ++k)							\
				calc->data[j-1][k] = calc->data[j-1][k]							\
							- (calc->data[i-1][k]*factor);						\
		}																		\
	}																			\
	/*normalize diag*/															\
	for(uint32_t i = 0; i < calc->size_y; ++i)									\
	{																			\
		if(calc->data[i][i] != 1)												\
		{																		\
			type factor = 1/calc->data[i][i];									\
			for(uint32_t k = 0; k < calc->size_x; ++k)							\
				calc->data[i][k] *=  factor;									\
		}																		\
	}																			\
	/*extract inverse matrix*/													\
	struct_name* result = struct_name##_construct(m->size_x, m->size_y);		\
	for(uint32_t i = 0; i < result->size_x; ++i)								\
		for(uint32_t j = 0; j < result->size_y; ++j)							\
			result->data[i][j] = calc->data[i][j+m->size_y];					\
	struct_name##_free(calc);													\
	return result;																\
}


#endif /* MATRIX_MATH_H_ */
