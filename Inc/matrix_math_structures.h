#ifndef MATRIX_MATH_STRUCTURES_H_
#define MATRIX_MATH_STRUCTURES_H_

#include "matrix_math.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-function"
GENERATE_MATRIX_MATH(Matrixf, float)
#pragma GCC diagnostic pop

Matrixf* get_RPY(float RadX, float RadY, float RadZ);
void get_RPY_s(float RadX, float RadY, float RadZ, Matrixf* result);

#endif /* MATRIX_MATH_STRUCTURES_H_ */
