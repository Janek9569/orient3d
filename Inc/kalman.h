#ifndef KALMAN_H_
#define KALMAN_H_

#include "matrix_math_structures.h"

typedef struct _Step_info
{
	Matrixf* xPost;
	Matrixf* pPost;
}Step_info;

typedef struct _Kalman_info
{
	Matrixf* A;
	Matrixf* B;
	Matrixf* H;
	Matrixf* R;
	Matrixf* q;
	Matrixf* W;

//for computations
	Matrixf* y;
	Matrixf* u;

	Step_info step_info;
} Kalman_info;

typedef struct _kalman
{
	Kalman_info info;
	void (*update_time) (void* ,float);
	void (*step)(Matrixf*, Matrixf*,void*);
} Kalman;

void kalman_destruct(Kalman* k);
Kalman* kalman_construct(Kalman_info kinfo, void (*update_time) (void*, float));
void kalman_step(Matrixf* u, Matrixf* y, void* kalman);

#endif
