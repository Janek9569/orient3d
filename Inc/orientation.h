#ifndef ORIENTATION_H_
#define ORIENTATION_H_
#include "kalman.h"
#include "MEMS.h"

typedef struct
{
	float roll_rad, roll_deg;
	float pitch_rad, pitch_deg;
}Orient2D;

Kalman_info orientation_create_rp_variables(float dt, float alfa, float accwar[2]);
void orientation_update_rp(Kalman* _k, float dt);
void orientation_rp_step(MEMS* mems, Kalman* k);
Orient2D orientation_get_rpy(Kalman* k);



#endif /* ORIENTATION_H_ */
