#include "kalman.h"

void kalman_step(Matrixf* u, Matrixf* y, void* kalman)
{
	Step_info* s_info = &((Kalman*)kalman)->info.step_info;
	Kalman_info* info = &((Kalman*)kalman)->info;

	//Q = q*W*qT
	Matrixf* qT = Matrixf_transpose(info->q);
	Matrixf* Q = Matrixf_mult_m(3,info->q,info->W,qT);

	Matrixf_free(qT);
	//xPrio = A*xPost+B*u
	Matrixf* AxPost = Matrixf_mult(info->A,s_info->xPost);
	Matrixf* Bu = Matrixf_construct(1,info->A->size_y);

	if(u != NULL)
	{
		Matrixf_free(Bu);
		Bu = Matrixf_mult(info->B,u);
	}

	Matrixf* xPrio = Matrixf_add(AxPost,Bu);

	Matrixf_free(AxPost);Matrixf_free(Bu);
	//pPrio = A*pPost*AT+Q
	Matrixf* AT = Matrixf_transpose(info->A);
	Matrixf* ApPostAT = Matrixf_mult_m(3,info->A,s_info->pPost,AT);
	Matrixf* pPrio = Matrixf_add(ApPostAT,Q);

	Matrixf_free(AT);Matrixf_free(ApPostAT);

	//K = pPrio * HT * (H * pPrio * HT + R)^-1
	Matrixf* HT = Matrixf_transpose(info->H);

	Matrixf* HpPrioHT = Matrixf_mult_m(3,info->H,pPrio,HT);
	Matrixf* HpPrioHTpR = Matrixf_add(HpPrioHT,info->R);
	Matrixf* inv = Matrixf_inv(HpPrioHTpR);
	Matrixf* K = Matrixf_mult_m(3,pPrio,HT,inv);
	Matrixf_free(HpPrioHT);Matrixf_free(HpPrioHTpR);Matrixf_free(inv);Matrixf_free(HT);

	//newX = xPrio + K * (y - H * xPrio)
	Matrixf* HxPrio = Matrixf_mult(info->H, xPrio);
	Matrixf* ymHxPrio = Matrixf_substract(y,HxPrio);
	Matrixf* KymHxPrio = Matrixf_mult(K,ymHxPrio);
	Matrixf* newX = Matrixf_add(xPrio,KymHxPrio);

	Matrixf_free(HxPrio);Matrixf_free(ymHxPrio);Matrixf_free(KymHxPrio);Matrixf_free(xPrio);

	//newP = (I-K*H)*pPrio
	Matrixf* KH = Matrixf_mult(K,info->H);
	Matrixf* I = Matrixf_I(KH->size_x);
	Matrixf* ImKH = Matrixf_substract(I,KH);
	Matrixf* newP = Matrixf_mult(ImKH,pPrio);//hard f

	Matrixf_free(KH);Matrixf_free(I);Matrixf_free(ImKH);
	Matrixf_free(Q);Matrixf_free(pPrio);Matrixf_free(K);

	Matrixf_free(s_info->pPost); Matrixf_free(s_info->xPost);
	s_info->pPost = newP; s_info->xPost = newX;
}

Kalman* kalman_construct(Kalman_info kinfo,void (*update_time) (void*, float))
{
	Kalman* newKalman = pvPortMalloc(sizeof(Kalman));
	newKalman->update_time = update_time;
	newKalman->info = kinfo;
	newKalman->step = kalman_step;
	return newKalman;
}

void kalman_destruct(Kalman* k)
{
	if(k == NULL)
		return;

	Matrixf_free(k->info.A);
	Matrixf_free(k->info.B);
	Matrixf_free(k->info.H);
	Matrixf_free(k->info.R);
	Matrixf_free(k->info.q);
	Matrixf_free(k->info.W);
	Matrixf_free(k->info.R);
	Matrixf_free(k->info.y);
	Matrixf_free(k->info.u);

	Matrixf_free(k->info.step_info.pPost);
	Matrixf_free(k->info.step_info.xPost);
	vPortFree(k);
}


