#include "stm32f3xx_hal.h"
#include "stm32f3xx.h"
#include "main.h"
#include "global_utils.h"
#include "usb_device.h"
#include "usbd_cdc_if.h"
#include "system_clock.h"
#include "FreeRTOS.h"
#include "task.h"
#include "tasks.h"
#include "MEMS.h"

static uint8_t _is_OS_turned_on = false;
void osbl_main()
{
	HAL_Init();
	SystemClock_Config();
	TaskHandle_t t_resource_allocator;
	xTaskCreate(osbl_resource_allocator, "RES_ALLOC", 1024, (void*)0, 6, &t_resource_allocator);
	vTaskStartScheduler();
}

void osbl_resource_allocator(void* argument)
{
	_is_OS_turned_on = true;

	MEMS_init();
	MX_USB_DEVICE_Init();

	vTaskDelay(3000);

	for ever
	{
		vTaskDelay(3000);
		CDC_Transmit_FS((uint8_t*)"SIEMA\r\n", 7);
	}

	for ever vTaskDelay(WAIT_FOR_EVER);
}

/*****INTERRUPTS*****/
extern void xPortSysTickHandler( void );
void SysTick_Handler(void)
{
	HAL_IncTick();
	HAL_SYSTICK_IRQHandler();

	if(_is_OS_turned_on)
		xPortSysTickHandler();
}








extern PCD_HandleTypeDef hpcd_USB_FS;
void USB_HP_CAN_TX_IRQHandler(void)
{
	HAL_PCD_IRQHandler(&hpcd_USB_FS);
}

void USB_LP_CAN_RX0_IRQHandler(void)
{
	HAL_PCD_IRQHandler(&hpcd_USB_FS);
}
