#include "matrix_math_structures.h"

Matrixf* get_RPY(float RadX, float RadY, float RadZ)
{
	Matrixf* rot = Matrixf_construct(3,3);
	rot->data[0][0] = cosf(RadZ)*cosf(RadY);
	rot->data[1][0] = sinf(RadZ)*cosf(RadY);
	rot->data[2][0] = -1*sinf(RadY);

	rot->data[0][1] = cosf(RadZ)*sinf(RadY)*sinf(RadX) - sinf(RadZ)*cosf(RadX);
	rot->data[1][1] = sinf(RadZ)*sinf(RadY)*sinf(RadX) + cosf(RadZ)*cosf(RadX);
	rot->data[2][1] = cosf(RadY)*sinf(RadX);

	rot->data[0][2] = cosf(RadZ)*sinf(RadY)*cosf(RadX) + sinf(RadZ)*sinf(RadX);
	rot->data[1][2] = sinf(RadZ)*sinf(RadY)*cosf(RadX) - cosf(RadZ)*sinf(RadX);
	rot->data[2][2] = cosf(RadY)*cosf(RadX);

	return rot;
}

void get_RPY_s(float RadX, float RadY, float RadZ, Matrixf* result)
{
	result->data[0][0] = cosf(RadZ)*cosf(RadY);
	result->data[1][0] = sinf(RadZ)*cosf(RadY);
	result->data[2][0] = -1*sinf(RadY);

	result->data[0][1] = cosf(RadZ)*sinf(RadY)*sinf(RadX) - sinf(RadZ)*cosf(RadX);
	result->data[1][1] = sinf(RadZ)*sinf(RadY)*sinf(RadX) + cosf(RadZ)*cosf(RadX);
	result->data[2][1] = cosf(RadY)*sinf(RadX);

	result->data[0][2] = cosf(RadZ)*sinf(RadY)*cosf(RadX) + sinf(RadZ)*sinf(RadX);
	result->data[1][2] = sinf(RadZ)*sinf(RadY)*cosf(RadX) - cosf(RadZ)*sinf(RadX);
	result->data[2][2] = cosf(RadY)*cosf(RadX);
}
