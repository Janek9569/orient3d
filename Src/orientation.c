#include "orientation.h"
#include "global_utils.h"

void orientation_deamon(void* argument)
{
	static Kalman_info rp_kalman_info;
	static Kalman* rp_kalman;
	static float accWar[] = {0.3, 0.3, 0.3};
	MEMS* mems = MEMS_init();

	rp_kalman_info = orientation_create_rp_variables(0.1, 0.001, accWar);
	rp_kalman = kalman_construct(rp_kalman_info, orientation_update_rp);
	for ever
	{
		rp_kalman->update_time(rp_kalman, 0.01);
		orientation_rp_step(mems, rp_kalman);
		vTaskDelay(10);
	}
}

void orientation_rp_step(MEMS* mems, Kalman* k)
{
	Matrixf* y = k->info.y;
	float ax = mems->get_acc(mems, X);
	float ay = mems->get_acc(mems, Y);
	float az = mems->get_acc(mems, Z);

	float pitch_rad = -(atan2(ax,sqrt(ay*ay+az*az)));
	float roll_rad = atan2(ax, az);

	y->data[X][0] = roll_rad;
	y->data[Y][0] = pitch_rad;

	Matrixf* u = k->info.u;
	u->data[X][0] = mems->get_gyr(mems, X);
	u->data[Y][0] = mems->get_gyr(mems, Y);
	k->step(u,y,k);
}

void orientation_update_rp(Kalman* k, float dt)
{
	float** d = k->info.A->data;
	d[0][2] = -dt;
	d[1][3] = -dt;

	d = k->info.B->data;
	d[0][0]=dt;
	d[1][1]=dt;
}

Orient2D orientation_get_rp(Kalman* k)
{
	Orient2D o;
	o.roll_rad = k->info.step_info.xPost->data[X][0];
	o.roll_deg = (k->info.step_info.xPost->data[X][0]*180.0)/M_PI;
	o.pitch_rad = k->info.step_info.xPost->data[Y][0];
	o.pitch_deg = (k->info.step_info.xPost->data[Y][0]*180.0)/M_PI;
	return o;
}

Kalman_info orientation_create_rp_variables(float dt, float alfa, float accwar[2])
{
	float** d;
	Matrixf* A = Matrixf_construct(4,4);
	Matrixf* B = Matrixf_construct(2,4);
	Matrixf* H = Matrixf_construct(4,2);
	Matrixf* R = Matrixf_construct(2,2);
	Matrixf* q = Matrixf_construct(2,4);
	d = A->data;
	d[0][0] = 1;	d[0][2] = -dt;
	d[1][1] = 1;	d[1][3] = -dt;
	d[2][2] = 1;
	d[3][3] = 1;
	d = H->data;
	d[0][0] = 1;	d[1][1] = 1;
	d = R->data;
	d[0][0] = accwar[0]*accwar[0];
	d[1][1] = accwar[1]*accwar[1];
	d = q->data;
	d[0][0] = 1;
	d[1][1] = 1;
	d = B->data;
	d[0][0]=dt;
	d[1][1]=dt;

	Matrixf* I = Matrixf_I(q->size_x);
	Matrixf* W = Matrixf_scalar(I, alfa);
	Matrixf_free(I);

	Matrixf* y = Matrixf_construct(1,2);
	Matrixf* u = Matrixf_construct(1,2);

	Matrixf* pPost = Matrixf_construct(3,6);
	Matrixf* xPost = Matrixf_construct(1,6);

	Kalman_info result =
	{
		.A = A,
		.B = B,
		.H = H,
		.R = R,
		.q = q,
		.W = W,
		.y = y,
		.u = u,
		.step_info =
		{
			xPost,
			pPost
		}
	};
	return result;
}
