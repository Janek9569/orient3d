#include "i2c.h"
#include "global_utils.h"
#include "stm32f3xx_hal.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"

I2C_HandleTypeDef i2c_handle;
DMA_HandleTypeDef i2c_dma_rx;
DMA_HandleTypeDef i2c_dma_tx;
static SemaphoreHandle_t i2c_sem;

static uint8_t i2c_lock_sem(uint32_t timeout)
{
	return xSemaphoreTake( i2c_sem, timeout ) == pdTRUE;
}

static void i2c_release_sem()
{
	xSemaphoreGive(i2c_sem);
}

static void i2c_release_sem_ISR()
{
	static BaseType_t xHigherPriorityTaskWoken;
	xSemaphoreGiveFromISR(i2c_sem, &xHigherPriorityTaskWoken);
}

void i2c_init()
{
	static uint8_t was_initialised = false;
	if(was_initialised)
		return;

	__HAL_RCC_DMA1_CLK_ENABLE();
	HAL_NVIC_SetPriority(DMA1_Channel4_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(DMA1_Channel4_IRQn);
	HAL_NVIC_SetPriority(DMA1_Channel5_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(DMA1_Channel5_IRQn);

	__HAL_RCC_I2C2_CLK_ENABLE();
	i2c_handle.Instance = I2C2;
	i2c_handle.Init.Timing = 0x2000090E;
	i2c_handle.Init.OwnAddress1 = 0;
	i2c_handle.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
	i2c_handle.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
	i2c_handle.Init.OwnAddress2 = 0;
	i2c_handle.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
	i2c_handle.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
	i2c_handle.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;

	ASSERT(HAL_I2C_Init(&i2c_handle) == HAL_OK);
	ASSERT(HAL_I2CEx_ConfigAnalogFilter(&i2c_handle, I2C_ANALOGFILTER_ENABLE) == HAL_OK);
	ASSERT(HAL_I2CEx_ConfigDigitalFilter(&i2c_handle, 0) == HAL_OK);

	i2c_sem = xSemaphoreCreateBinary();
	i2c_release_sem();
}

void HAL_I2C_MspInit(I2C_HandleTypeDef* i2cHandle)
{
	 __HAL_RCC_GPIOF_CLK_ENABLE();
	 __HAL_RCC_GPIOA_CLK_ENABLE();
	 GPIO_InitTypeDef GPIO_InitStruct;

	 GPIO_InitStruct.Pin = GP(9) | GP(10);
	 GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
	 GPIO_InitStruct.Pull = GPIO_PULLUP;
	 GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	 GPIO_InitStruct.Alternate = GPIO_AF4_I2C2;
	 HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	 __HAL_RCC_I2C2_CLK_ENABLE();

	 i2c_dma_rx.Instance = DMA1_Channel5;
	 i2c_dma_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
	 i2c_dma_rx.Init.PeriphInc = DMA_PINC_DISABLE;
	 i2c_dma_rx.Init.MemInc = DMA_MINC_ENABLE;
	 i2c_dma_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
	 i2c_dma_rx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
	 i2c_dma_rx.Init.Mode = DMA_NORMAL;
	 i2c_dma_rx.Init.Priority = DMA_PRIORITY_LOW;
	 ASSERT(HAL_DMA_Init(&i2c_dma_rx) != HAL_OK);
     __HAL_LINKDMA(i2cHandle,hdmarx,i2c_dma_rx);

     i2c_dma_tx.Instance = DMA1_Channel4;
     i2c_dma_tx.Init.Direction = DMA_MEMORY_TO_PERIPH;
     i2c_dma_tx.Init.PeriphInc = DMA_PINC_DISABLE;
     i2c_dma_tx.Init.MemInc = DMA_MINC_ENABLE;
     i2c_dma_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
     i2c_dma_tx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
     i2c_dma_tx.Init.Mode = DMA_NORMAL;
     i2c_dma_tx.Init.Priority = DMA_PRIORITY_LOW;
	 ASSERT(HAL_DMA_Init(&i2c_dma_tx) != HAL_OK);
	 __HAL_LINKDMA(i2cHandle,hdmatx,i2c_dma_tx);
}

void i2c_deinit()
{
	__HAL_RCC_I2C2_CLK_DISABLE();
	HAL_GPIO_DeInit(GPIOA, GP(9) | GP(10));

    HAL_DMA_DeInit(i2c_handle.hdmarx);
    HAL_DMA_DeInit(i2c_handle.hdmatx);
}

void i2c_write_mem(uint16_t dev_addr, uint16_t mem_addr, uint16_t mem_size, uint8_t* data, uint16_t len)
{
	i2c_lock_sem(WAIT_FOR_EVER);
	while(HAL_I2C_Mem_Write_DMA(&i2c_handle, dev_addr, mem_addr, mem_size, data, len) != HAL_OK)
		vTaskDelay(1);
	i2c_lock_sem(WAIT_FOR_EVER);
	i2c_release_sem();
}

void i2c_read_mem(uint16_t dev_addr, uint16_t mem_addr, uint16_t mem_size, uint8_t* data, uint16_t len)
{
	i2c_lock_sem(WAIT_FOR_EVER);
	while(HAL_I2C_Mem_Read_DMA(&i2c_handle, dev_addr, mem_addr, mem_size, data, len)!= HAL_OK)
		vTaskDelay(1);
	i2c_lock_sem(WAIT_FOR_EVER);
	i2c_release_sem();
}

/*****INTS*****/
void DMA1_Channel5_IRQHandler(void)
{
	HAL_DMA_IRQHandler(&i2c_dma_rx);
	i2c_handle.State = HAL_I2C_STATE_READY;
	i2c_release_sem_ISR();
}

void DMA1_Channel4_IRQHandler(void)
{
	HAL_DMA_IRQHandler(&i2c_dma_tx);
	i2c_handle.State = HAL_I2C_STATE_READY;
	i2c_release_sem_ISR();
}



