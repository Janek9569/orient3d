#include "MEMS.h"
#include "global_utils.h"
#include "FreeRTOS.h"
#include <math.h>
#include <string.h>

#define MEMS_ADDR 0x68
#define PWR_MGMT_1 0x6B
#define ACC_CONFIG_ADDR 0x1C
#define GYRO_CONFIG_ADDR 0x1B
#define WHO_AM_I_REG 0x75
#define G_ACC 9.80665
#define DEG2RAD M_PI/180
#define ACC_X_H 0x3B					// Adres rejestru bardziej znaczącego bajta osi X akcelerometru
#define GYRO_X_H 0x43					// Adres rejestru bardziej znaczącego bajta osi X żyroskopu
#define MAGN_REG_WHOAMI 0x00
#define MAGN_WHOAMI 0x48
#define MAGNET_X_H 0x03
#define MAGNET_CNTL1 0x0A
#define MAGNET_CNTL2 0x0B
#define MEMS_INT_ENABLE_REG     0x38
#define MEMS_MOT_THR 0x1F
#define MEMS_INT_PIN_CFG 0x37
//CONFIG VALUES
#define ACC_RESOLUTION_CONFIG 0x18		// 0x00 - 2g ; 0x08 - 4g ; 0x10 - 8g ; 0x18 - 16g
#define GYRO_RESOLUTION_CONFIG 0x18		// 0x00 - 250dps ; 0x08 - 500dps ; 0x10 - 1000dps ; 0x18 - 2000dps
#define EXPECTED_WHO_AM_I_VALUE 0x71
#define ACC_AVERANGE_LOOPS 1000
#define ACC_RESOLUTION 16.0				// [g]
#define GYRO_RESOLUTION 2000.0			// [dps]
#define MAGN_RESOLUTION 4912			// [uT]
#define MAGN_SCALAR 0.15				// [uT]

/***** PRIVATE FUNCTIONS *****/
static void _mems_read_raw_data(void* _mems)
{
	MEMS* mems = (MEMS*)_mems;
	uint8_t data[6] = {0,0,0,0,0,0};

	//get raw acc data
	i2c_read_mem(MEMS_ADDR, ACC_X_H, 1, data, 6);
	int16_t X_acc=(data[0]<<8 | data[1]);
	mems->acc[X] = ((float)X_acc*ACC_RESOLUTION*G_ACC)/(float)INT16_MAX;
	int16_t Y_acc=(data[2]<<8 | data[3]);
	mems->acc[Y] = ((float)Y_acc*ACC_RESOLUTION*G_ACC)/(float)INT16_MAX;
	int16_t Z_acc=(data[4]<<8 | data[5]);
	mems->acc[Z] = ((float)Z_acc*ACC_RESOLUTION*G_ACC)/(float)INT16_MAX;

	// get raw gyro data
	i2c_read_mem(MEMS_ADDR, GYRO_X_H, 1, data, 6);
	int16_t X_as=(data[0]<<8 | data[1]);
	mems->gyro[X] = ((float)X_as*GYRO_RESOLUTION * DEG2RAD)/(float)INT16_MAX;
	int16_t Y_as=(data[2]<<8 | data[3]);
	mems->gyro[Y] = ((float)Y_as*GYRO_RESOLUTION * DEG2RAD)/(float)INT16_MAX;
	int16_t Z_as=(data[4]<<8 | data[5]);
	mems->gyro[Z] = ((float)Z_as*GYRO_RESOLUTION * DEG2RAD)/(float)INT16_MAX;

	//get raw magnet data
	i2c_read_mem(mems->magnet_addr, MAGNET_X_H, 1, data, 6);
	int16_t X_m=(data[0]<<8 | data[1]);
	mems->magnet[X] = ((float)X_m*MAGN_SCALAR);
	int16_t Y_m=(data[2]<<8 | data[3]);
	mems->magnet[Y] = ((float)Y_m*MAGN_SCALAR);
	int16_t Z_m=(data[4]<<8 | data[5]);
	mems->magnet[Z] = ((float)Z_m*MAGN_SCALAR);
}

static float _mems_get_acc(void* _mems, Orient orient)
{
	MEMS* mems = (MEMS*)_mems;
	return (mems->acc[orient]- mems->acc_zero[orient]);
}

static float _mems_get_gyr(void* _mems, Orient orient)
{
	MEMS* mems = (MEMS*)_mems;
	return (mems->gyro[orient]- mems->gyro_zero[orient]);
}

static float _mems_get_magnet(void* _mems, Orient orient)
{
	MEMS* mems = (MEMS*)_mems;
	return (mems->magnet[orient]- mems->magnet_zero[orient]);
}

MEMS* MEMS_init()
{
	static uint8_t is_initialized = 0;
	ASSERT(is_initialized == 0);

	i2c_init();
	is_initialized = 1;

    //configure mpu
	uint8_t reg_value=0x00;
	i2c_write_mem(MEMS_ADDR, PWR_MGMT_1, 1, &reg_value, 1);

	//i2c communication test
	i2c_read_mem(MEMS_ADDR, WHO_AM_I_REG, 1, &reg_value, 1);
	ASSERT(reg_value == EXPECTED_WHO_AM_I_VALUE);

	//resolution
	reg_value=ACC_RESOLUTION_CONFIG;
	i2c_write_mem(MEMS_ADDR, ACC_CONFIG_ADDR, 1, &reg_value, 1);

	reg_value=GYRO_RESOLUTION_CONFIG;
	i2c_write_mem(MEMS_ADDR, GYRO_CONFIG_ADDR, 1, &reg_value, 1);

	//initialize MEMS struct
	MEMS* mems = (MEMS*)pvPortMalloc(sizeof(MEMS));

	//find magnetometer
	mems->magnet_addr = 0xFF;
    for (uint8_t magnet_addr = 0x0C; magnet_addr <= 0x0F; ++magnet_addr)
    {
    	uint8_t dev_id = 0;
    	i2c_read_mem(magnet_addr, MAGN_REG_WHOAMI, 1, &dev_id, 1);
        if (dev_id == MAGN_WHOAMI)
        {
        	mems->magnet_addr = magnet_addr;
        }
    }
    ASSERT(mems->magnet_addr != 0xFF);

    //configure magnetometer
    //set cntl reg -> set continuous measurement 1 (8Hz) and 16bit output
    reg_value = 0x12;
    i2c_write_mem(mems->magnet_addr, MAGNET_CNTL1, 1, &reg_value, 1);

#ifdef MEMS_HANDLED_WITH_INTS
    //enable MPU interrupts
    reg_value = 0x40;
    i2c_write_mem(MEMS_ADDR, MEMS_INT_ENABLE_REG, 1, &reg_value, 1);

    //set threshold
    reg_value = 3;
    i2c_write_mem(MEMS_ADDR, MEMS_MOT_THR, 1, &reg_value, 1);

    //int pin
    reg_value = 0;//active - H , PP, 50us pulse
    i2c_write_mem(MEMS_ADDR, MEMS_INT_PIN_CFG, 1, &reg_value, 1);

    //TO DO configure int period
#endif

    //initialize MEMS struct continuation
	mems->read_raw_data = _mems_read_raw_data;
	mems->get_acc = _mems_get_acc;
	mems->get_gyr = _mems_get_gyr;
	mems->get_magnet = _mems_get_magnet;

	float acc_sum[ORIENT_LAST];
	memset(acc_sum, 0x00, ORIENT_LAST*sizeof(float));

	for(uint16_t i = 0; i < ACC_AVERANGE_LOOPS; ++i)
	{
		mems->read_raw_data(mems);
		acc_sum[X] += mems->acc[X];
		acc_sum[Y] += mems->acc[Y];
		acc_sum[Z] += mems->acc[Z];
	}
	mems->acc_zero[X] = acc_sum[X] / (float)ACC_AVERANGE_LOOPS;
	mems->acc_zero[Y] = acc_sum[Y] / (float)ACC_AVERANGE_LOOPS;
	mems->acc_zero[Z] = acc_sum[Z] / (float)ACC_AVERANGE_LOOPS - G_ACC; // TO DO: WHY DID I THIS IN PAST ?

	mems->read_raw_data(mems);
	mems->gyro_zero[X] = mems->gyro[X];
	mems->gyro_zero[Y] = mems->gyro[Y];
	mems->gyro_zero[Z] = mems->gyro[Z];

	mems->magnet_zero[X] = mems->magnet[X];
	mems->magnet_zero[Y] = mems->magnet[Y];
	mems->magnet_zero[Z] = mems->magnet[Z];

	return mems;
}



