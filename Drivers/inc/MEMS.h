#ifndef INC_MEMS_H_
#define INC_MEMS_H_
#include "i2c.h"

typedef enum _Orient
{
	X,
	Y,
	Z,
	ORIENT_LAST
}Orient;

typedef struct
{
	float acc[3]; // m/s^2
	float acc_zero[3]; // m/s^2
	float gyro[3]; // rad/s
	float gyro_zero[3]; //rad/s
	float magnet[3];
	float magnet_zero[3];

	float acc_normalized[3];// m/s^2

	float (*get_acc) (void*, Orient);
	float (*get_gyr) (void*, Orient);
	float (*get_magnet) (void*, Orient);
	void (*read_raw_data) (void*);

	uint8_t magnet_addr;
} MEMS;

MEMS* MEMS_init();


#endif
